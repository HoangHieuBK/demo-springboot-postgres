package com.example.demo.controller;

import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ResourceException;
import com.example.demo.model.Answer;
import com.example.demo.repository.AnswerRepository;
import com.example.demo.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Optional;

@RestController
public class AnswerController {

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @GetMapping("/questions/{questionId}/answers")
    public List<Answer> getAnswersByQuestionId(@PathVariable Long questionId) {
        List<Answer> answers = answerRepository.findByQuestionId(questionId);
        if (answers.size() == 0) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found");
        }
        return answers;
    }

    @PostMapping("/questions/{questionId}/answers")
    public Answer addAnswer(@PathVariable Long questionId,
                            @Valid @RequestBody Answer answer) {
        if (answerRepository.existsByText(answer.getText())) {
            throw new ConflictException("Answer has already existed with text " + answer.getText());
        }
        return questionRepository.findById(questionId)
                .map(question -> {
                    answer.setQuestion(question);
                    return answerRepository.save(answer);
                }).orElseThrow(() -> new ResourceException("Question not found with id " + questionId));
    }

    @PutMapping("/questions/{questionId}/answers/{answerId}")
    public Answer updateAnswer(@PathVariable Long questionId,
                               @PathVariable Long answerId,
                               @Valid @RequestBody Answer answerRequest) {
        if (!questionRepository.existsById(questionId)) {
            throw new ResourceException("Question not found with id " + questionId);
        }
        return answerRepository.findById(answerId)
                .map(answer -> {
                    answer.setText(answerRequest.getText());
                    return answerRepository.save(answer);
                }).orElseThrow(() -> new ResourceException("Answer not found with id " + answerId));
    }

    @DeleteMapping("/questions/{questionId}/answers/{answerId}")
    public ResponseEntity<?> deleteAnswer(@PathVariable Long questionId,
                                          @PathVariable Long answerId) {
        if (!questionRepository.existsById(questionId)) {
            throw new ResourceException("Question not found with id " + questionId);
        }

        return answerRepository.findById(answerId)
                .map(answer -> {
                    answerRepository.delete(answer);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceException("Answer not found with id " + answerId));

    }

    @DeleteMapping("/questions/{questionId}/delete-answers")
    @Transactional
    public ResponseEntity<?> deleteMultipleAnswer(@PathVariable Long questionId,
                                                  @Valid @RequestBody List<Long> ids) {
//        if (!questionRepository.existsById(questionId)) {
//            throw new ResourceException("Question not found with id " + questionId);
//        }
//
//        for (int i = 0; i < ids.length; ++i) {
//            Optional<Answer> answerOpt = answerRepository.findById(ids[i]);
//            if (!answerOpt.isPresent()) {
//                throw new ResourceException("Answer not found with id " + ids[i]);
//            }
//            answerRepository.delete(answerOpt.get());
//        }
        int res = answerRepository.deleteMultipleAnswers(ids);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }
}
