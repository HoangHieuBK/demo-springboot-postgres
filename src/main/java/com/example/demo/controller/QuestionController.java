package com.example.demo.controller;


import com.example.demo.exception.ResourceException;
import com.example.demo.model.Question;
import com.example.demo.repository.QuestionRepository;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/questions")
public class QuestionController {

    @Autowired
    private QuestionRepository questionRepository;

    @GetMapping()
    public List<Question> getQuestions(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "5") Integer pageSize,
            @RequestParam(defaultValue = "title") String orderBy
    ) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(orderBy));
        Page<Question> pageResult = questionRepository.findAll(pageable);
        if(pageResult.hasContent()) {
            return pageResult.getContent();
        } else {
            return new ArrayList<Question>();
        }
    }

//    @GetMapping("/filter")
//    public List<Question> filterQuestion(@RequestParam String title, @RequestParam String description, @RequestParam String categoryName) {
//        return questionRepository.filterQuestion("%" + title + "%", "%" + description + "%", "%" + categoryName + "%");
//    }

    @GetMapping("/filter")
    public Page<Question> filterQuestions(
            @Join(path = "category", alias = "cate")
            @Or({
                    @Spec(path = "cate.name", params = "categoryName", spec = Like.class),
                    @Spec(path = "title", spec = Like.class),
                    @Spec(path = "description", spec = Like.class)
            }) Specification<Question> questionSpec, Pageable pageable) {
         return questionRepository.findAll(questionSpec, pageable);
    }

    @PostMapping()
    public Question createQuestion(@Valid @RequestBody Question question) {
        if (questionRepository.existsByTitle(question.getTitle())) {
//            throw new HttpClientErrorException.BadRequest();
        }
        return questionRepository.save(question);
    }

    @PutMapping("/{questionId}")
    public Question updateQuestion(@PathVariable Long questionId,
                                   @Valid @RequestBody Question questionRequest) {
        return questionRepository.findById(questionId)
                .map(question -> {
                    question.setTitle(questionRequest.getTitle());
                    question.setDescription(questionRequest.getDescription());
                    return questionRepository.save(question);
                }).orElseThrow(() -> new ResourceException("Question not found with id " + questionId));
    }


    @DeleteMapping("/{questionId}")
    public ResponseEntity<?> deleteQuestion(@PathVariable Long questionId) {
        return questionRepository.findById(questionId)
                .map(question -> {
                    questionRepository.delete(question);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceException("Question not found with id " + questionId));
    }

}