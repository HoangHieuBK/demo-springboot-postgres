package com.example.demo.repository;

import com.example.demo.model.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long>, PagingAndSortingRepository<Question, Long> {
   Boolean existsByTitle(String title);

   @Query("SELECT que from Question que where que.title like :title or que.description like  :description or que.category.name like :categoryName")
   List<Question> filterQuestion(@Param("title") String title , @Param("description") String description, @Param("categoryName") String categoryName);

   Page<Question> findAll(Specification<Question> questionSpec, Pageable pageable);

}
