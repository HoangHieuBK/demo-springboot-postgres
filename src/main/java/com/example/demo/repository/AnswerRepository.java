package com.example.demo.repository;

import com.example.demo.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {

    @Query("SELECT anws FROM Answer anws WHERE anws.question.id = :questionId")
    List<Answer> findByQuestionId(@Param("questionId") Long questionId);

    @Query("SELECT anw from Answer anw where anw.id = :answerId")
    Optional<Answer> findByAnswerId(@Param("answerId") Long answerId);

    boolean existsByText(String text);

    @Modifying
    @Query("DELETE from Answer anw where anw.id in :ids")
    int deleteMultipleAnswers(@Param("ids") List<Long> ids);
}
